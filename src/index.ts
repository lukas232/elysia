import { Elysia } from "elysia";
import mysql from "mysql2/promise";

const pool = mysql.createPool({
    host: '127.0.0.1',
    user: 'root',
    password: 'root',
    database: 'performance1',
});

const query = 'SELECT count(*) FROM system_event_logs';

const sleep = (seconds: number) => {
    const start = Date.now()
    let iteration = 0
    let count = 0
    while (true) {
        iteration++
        count++
        if (iteration == 10000000) {
            if (((Date.now() - start)/1000) > seconds) {
                console.log(count)
                break
            }
            iteration = 0
        }
    }
}


const app = new Elysia()
    .get('/', async () => {

        const start = Date.now()
        const [results] = await pool.execute(query);
        return (Date.now() - start) / 1000
    })
    .get('blank', () => {
        return 'elysia backend'
    })
    .get('sleep', async () => {
        console.log('start')
        await sleep(10)
        console.log('end')

    })
    .listen(3000);

console.log(` Elysia is running at localhost:3000`);
